# My Git knowledge base

[TOC]


## Welcome!

Welcome to my personal Git knowledge base!

:wave: :smile: :v:

Current open source repos I contribute to:

- [The Good Docs Project](https://gitlab.com/tgdp)
- [Write the Docs](https://github.com/writethedocs)
- [The Salt Project Docs](https://gitlab.com/saltstack/open/docs)
- [The Salt Project](https://github.com/saltstack/salt)


## Helpful resources about the basics
At the Salt Project, we use the fork and clone method and this is a resource I use a lot: [Using the fork-and-branch Git workflow](https://blog.scottlowe.org/2015/01/27/using-fork-branch-git-workflow/)

I also like this tutorial: [Learn Git Branching](https://learngitbranching.js.org/?locale=en_US)


## My most common Git tasks


### Rename a branch
To rename the current branch: 

```
git branch -m new-branch-name
```

### Delete a branch
You need to make sure you’ve pulled the upstream changes that you merged. Sometimes after merging a branch, you can delete the branch on your forked repo (origin).

Then you need to delete it locally using:

```
git branch -d branch-name
```

If you didn’t delete the branch when it was merged, you can then delete it from your forked repo using:

```
git push origin :branch-name
```

To check all branches: 

```
git branch -a
```

To delete an upstream branch: 

```
git push origin --delete branch-name
```

If you get the error that it failed to push some refs to the branch, it could mean that the remote is not in the upstream repository. Use: 

```
git fetch -p origin
```

### Check out a new branch based on an upstream branch that isn't the default
Check out a branch and sync it with the upstream content: 

```
git checkout -b branch-name upstream/freeze
```

NOTE: Freeze here is an example of an upstream branch name.


## Vim commands

- To write and save: `:wq`
- To enter editing mode: `i` (for insert)
- To leave editing mode: `esc`


## Basic Git tasks

### Update my local repo
Note: This assumes you’ve set up your remotes. To check remotes: 

```
git remote -v
```

1. Use `cd` to navigate to your repo.

2. Make sure you’re on the branch you need! (Usually main)

3. Use the command: `git fetch`.

4. Use the command: `git pull upstream [branchName e.g. main]`

5. To sync between my local repositories: `git push origin [branchName e.g. main]`

6. If you need to commit a merge message in VIM, press `i` to enter insert mode. Once the line is correct, switch to Normal mode by pressing `Esc`, Then you can type `:wq`.

Troubleshooting:

- How to abandon local changes and just sync with the upstream branch: https://github.community/t5/How-to-use-Git-and-GitHub/Syncing-a-fork-leaves-me-one-commit-ahead-of-upstream-master/td-p/13555
- Resolving merge conflicts: https://help.github.com/en/articles/resolving-a-merge-conflict-using-the-command-line


### Push to my repo, then upstream
Make sure you’ve added and committed your changes.

1. Use this command: 

   ```
   git push origin [branchName e.g. my_feature]
   ```

2. Then go to your repo and open a pull request for the commit and have it reviewed by the necessary technical people.

   Skip the previous step if you’re fixing something related to a PR review. GitHub will automatically update the PR.

### Check out a branch
To see all branches:

```
git branch -a
```

Check out existing branch: 
```
git checkout branch-name
```

Check out and create new branch: 

```
git checkout -b branch-name
```
IMPORTANT NOTE: The branch you currently have checked out when you create a new branch is the branch it will base the changes from.


## Add and commit your changes
Before editing something, check out the branch you want to make the edits on. Usually this involves creating a new branch.

1. Use `git status` to check which files were modified.

2. Use `git add .` to stage all the files or or `git add name-of-file` to add a single file.

3. Use `git commit - m "My commit message"` to commit the files.

## Create a branch and push upstream
The command `git push -u origin <local_feature_branch>` will create the remote branch and set you up to track it.

## Create a branch with uncommitted changes
See [Stack Overflow](https://stackoverflow.com/questions/1394797/move-existing-uncommitted-work-to-a-new-branch-in-git).

The command is:

```
git switch -c <new-branch-name>
```

## Resolve merge conflicts
In the code editor, look at files with merge conflicts and find the conflicts. Then select either “ours” or “theirs.” (Best to use “theirs” as a default rule.)

Add your resolved conflict files to a commit and commit it.


## Merge a branch into the main (default) branch

1. Check out the branch you are merging into (develop, master, etc.).

2. Use git merge [feature-branch] to merge the files.

3. Resolve any merge conflicts.
